from datetime import datetime
import serial
import time
import argparse

parser = argparse.ArgumentParser(description='Time showing Program for DMD Screen')

parser.add_argument('--duino', default='/dev/ttyUSB0',
                    help="Clock controller (Arduino) connection target. Default: '/dev/ttyUSB0'\n")

parser.add_argument('--room', default='JMSS',
                    help="Name of room that clock is located in\n")

parser_args = parser.parse_args()


def init():
    init_count = 0
    while True:
        init_count += 1
        ab = " Init\n"
        duino.write(ab.encode())
        time.sleep(0.02)
        if init_count > 200:
            break

    time.sleep(0.1)
    ab = "  Init\n"
    duino.write(ab.encode())

    write = "  me! \n"
    duino.write(write.encode())
    time.sleep(2)
    write = " is! \n"
    duino.write(write.encode())
    time.sleep(2)
    write = "clock!\n"
    duino.write(write.encode())
    time.sleep(4)
    write = str(parser_args.room)
    write = "  " + "\n"
    duino.write(write.encode())
    time.sleep(5)

print("Using \"" + str(parser_args.duino) + "\" as arduino address")

print("\nAttempting to initialise device...")


try:
    duino = serial.Serial(
        port=str(parser_args.duino),
        baudrate=9600,
    )
    print("Success!")

except Exception as e:
    time.sleep(0.1)
    exit("Could not connect to arduino")

init()

time.sleep(5)
old_time = str(datetime.now())[11:16]
duino.write((" " + old_time + "\n").encode())

while True:
    if not old_time == str(datetime.now())[11:16]:
        old_time = str(datetime.now())[11:16]
        duino.write((" " + old_time + "\n").encode())
    else:
        time.sleep(0.95)
