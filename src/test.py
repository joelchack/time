import serial
import argparse
import time
import struct

parser = argparse.ArgumentParser(description='Test Program for DMD Screen')

parser.add_argument('--duino', default='/dev/ttyUSB0',
                    help="Vehicle connection target. Default: '/dev/ttyUSB0'\n")

parser_args = parser.parse_args()


print("Using \"" + str(parser_args.duino) + "\" as arduino address")


print("Attempting to initialise device...")

try:
    duino = serial.Serial(
        port=str(parser_args.duino),
        baudrate=9600,
    )
    print("\nSuccess!")

except Exception as e:
    print("\nFailed! Exception: " + str(e))
    time.sleep(0.1)
    print("Ignoring...")

while True:

    usr_in = str(input("\nPlease enter data to send to duino\n>> "))
    if "EXIT" in usr_in:
        duino.close()

    usr_in += "\n"

    duino.write(usr_in.encode())

